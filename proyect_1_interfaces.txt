PROYECTO DE DESARROLLO DE

INTERFACES #1

Temática de la propuesta

El tema de la aplicación 1 será: un reloj.

Trabajos a realizar
Prototipo de la aplicación

Decisiones a tomar:
1. Requerimientos funcionales de vuestra aplicación
2. Requerimientos no funcionales de la aplicación
3. Público objetivo de vuestra aplicación
4. Plataforma de vuestra aplicación: Móvil/Web/Escritorio
5. Software con el que vais a desarrollar el prototipo
6. Nombre de la aplicación
7. Diseño de la aplicación
8. Diseño de las funciones de la aplicación
9. Diseño de las interacciones y comunicaciones con el usuario
10. Política de seguridad y/o privacidad de la aplicación

Documentación

Todo el proceso de desarrollo de la aplicación deberá ser documentado incluyendo al menos:
1. Recopilación de las decisiones tomadas al respecto de los puntos señalados en la sección
anterior
2. Cuáles son los factores diferenciales entre vuestra aplicación y otros ejemplos que ya existen
en el mercado
3. Asignación de trabajos por cada miembro del equipo
4. Manual de usuario de vuestra aplicación
Presentación (fecha entrega – presentaciones 4 nov.)
Exposición oral por parte de uno de los miembros del equipo en la que se contemplarán al menos
los siguientes puntos teniendo una duración de al menos 30 minutos sin superar los 60 minutos:

1. Cuál fue la dinámica en la toma de decisiones respecto a la aplicación
2. Cómo se organizó el trabajo en equipo
3. Qué papel tuvo cada miembro del equipo en el trabajo
4. Descripción de:
a. Requerimientos funcionales de vuestra aplicación
b. Requerimientos no funcionales de la aplicación
c. Público objetivo de vuestra aplicación
d. Plataforma de vuestra aplicación: Móvil/Web/Escritorio
e. Software con el que vais a desarrollar el prototipo
f. Diseño de la aplicación
g. Diseño de las funciones de la aplicación
h. Diseño de las interacciones y comunicaciones con el usuario
i. Política de seguridad y/o privacidad de la aplicación
5. Muestra del prototipo de la aplicación
6. Defensa de los puntos diferenciales de vuestra aplicación con respecto a otras alternativas de
mercado

Evaluación
Parte 1 (Trabajo en grupo)

Cada miembro del grupo valorará a sus compañeros del 1 al 10 en los siguientes aspectos:
El alumno ______________
1. Ha sido capaz de comunicarse con el resto de los compañeros
2. Ha colaborado en la resolución de conflictos
3. Ha escuchado y aceptado las decisiones del grupo
4. Ha respetado al resto de los compañeros del grupo
5. Calidad en la ejecución de las tareas asignadas
La nota final será calculada en un 40% según las valoraciones de los compañeros y un 60% según la
valoración de la profesora

Parte 2 (Prototipado)

Para ser evaluado en esta segunda parte es requisito imprescindible aprobar la primera parte de la
evaluación. La nota será la misma para todos los miembros del equipo que hayan superado la
primera parte
1. Prototipo de la aplicación: 4 puntos
2. Documentación: 3 puntos
3. Presentación: 3 puntos
4. Implementación (opcional) +2 puntos
